<?php

namespace App\Providers;

use App\Contracts\UserServiceInterface;
use App\Models\Role;
use App\Services\UserService;
use App\Services\UserService2;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        UserServiceInterface::class => UserService::class
    ];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Gate::define(Role::ADMIN, function (User $user) {
            return $user->role->code == Role::ADMIN;
        });
        Gate::define(Role::TEAM_LEAD, function (User $user) {
            return $user->role->code == Role::TEAM_LEAD;
        });
        Gate::define(Role::RAZRAB, function (User $user) {
            return $user->role->code == Role::RAZRAB;
        });
        Gate::define(Role::ANALITIC, function (User $user) {
            return $user->role->code == Role::ANALITIC;
        });
        Gate::define(Role::OWNER, function (User $user) {
            return $user->role->code == Role::OWNER;
        });
    }
}
