<?php


namespace App\Services;


use App\Contracts\TeamServiceInterface;
use App\Models\Team;
use Illuminate\Support\Collection;

class TeamService implements TeamServiceInterface
{
    public function getAll()
    {
        return Team::with('roles')->get();
    }

    public function show($id)
    {
        $team = Team::find($id);
        return $team;
    }
}
