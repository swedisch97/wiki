<?php


namespace App\Services;


use App\Models\Project;

class ProjectService
{
    public function show($id){

        return Project::findOrFail($id);
    }
    public function getAll(){
        return Project::with('users')->get();
    }

}
