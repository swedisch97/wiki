<?php


namespace App\Services;


use App\Contracts\UserServiceInterface;
use App\Models\Project;
use App\Models\User;
use Illuminate\Support\Collection;

class UserService implements UserServiceInterface
{
    public function getAll()
    {
        return User::with('role')->get();
    }

    public function show($id)
    {
        $user = User::with('role')->findOrFail($id);
        return $user;
    }
    public function getUsersByRole($roleId){
        $role=User::query()->where('role_id','=',$roleId)->get();
        return $role;
    }
    public function getUsersInProjects($projectId){
        return Project::query()->where('project_id','=',$projectId)->get();
    }
}
