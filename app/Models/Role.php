<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    const  ADMIN = 'admin';
    const  TEAM_LEAD = 'team_lead';
    const  RAZRAB = 'razrab';
    const  ANALITIC = 'anal';
    const  OWNER = 'owner';

    protected $fillable = ['name', 'code'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
