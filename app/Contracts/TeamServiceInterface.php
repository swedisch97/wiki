<?php
namespace App\Contracts;


use Illuminate\Support\Collection;

interface TeamServiceInterface
{
    public function getAll();
    public function show($id);
}
