<?php


namespace App\Contracts;


use Illuminate\Support\Collection;

interface UserServiceInterface
{
    public function getAll();
    public function show($id);
}
