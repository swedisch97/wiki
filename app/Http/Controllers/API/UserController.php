<?php

namespace App\Http\Controllers\API;

use App\Exceptions\RoleIncorrect;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\UnauthorizedException;
use Laravel\Sanctum\PersonalAccessToken;

class UserController extends Controller
{
    private UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->userService->getAll();

        return $user;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->userService->show($id);
    }

    public function getUsersByRole($role_id){
        if (!Auth::check()) {
            throw new UnauthorizedException('Пользватель не авторизован');
        }

//        dd(Gate::check('can-show-roles'));

        if (!Gate::check(Role::ADMIN)) {
            throw new RoleIncorrect('роль не '. Role::ADMIN);
        }

       return $this->userService->getUsersByRole($role_id);
    }
    public function getToken(Request $request){

        $user = User::query()->where('email','=',$request->login)->first();



        // TODO: получать сущестующий
        //        $tokens = $user->tokens;
//        if ($tokens->isEmpty()){
//            $token=$user->createToken('api');
//            return $token->plainTextToken;
//        }

            $token=$user->createToken('api');
            return $token->plainTextToken;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (!Auth::check()) {
            throw new UnauthorizedException('Пользватель не авторизован');
        }

        $user = User::destroy($id);
        return response()->json([
            "data" => [
                "status" => (bool) $user
            ]
        ]);
    }
}
