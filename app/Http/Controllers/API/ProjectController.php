<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ProjectService;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    private ProjectService $projectService;

    public function __construct(ProjectService $projectService){
        $this->projectService = $projectService;
    }
    public function show($id){
        return $this->projectService->show($id);
    }
    public function index(){
        return $this->projectService->getAll();
    }

}
