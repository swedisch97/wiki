<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\TeamService;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    private TeamService $teamService;

    public function __construct(TeamService $teamService){
        $this->teamService=$teamService;
    }
    public function show($id){
       return $this->teamService->show($id);
    }
    public function index(){
        return $this->teamService->getAll();
    }

}
