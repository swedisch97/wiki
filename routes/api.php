<?php

use App\Http\Controllers\API\UserController;
use App\Http\Controllers\API\ProjectController;
use App\Http\Controllers\API\TeamController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//Route::get('custom',[UserController::class, 'index'])->name('custome_route');

Route::apiResource('users', UserController::class);
//Route::get('users',[UserController::class,'index']);
Route::apiResource('teams',TeamController::class)->middleware('auth:sanctum');

Route::middleware('auth:sanctum')->get('/roles/{roleId}',[UserController::class,'getUsersByRole']);
Route::post('users/token/',[UserController::class,'getToken']);
Route::apiResource('projects', ProjectController::class);

