<?php


namespace Database\Seeders;


use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    public function run()
    {
        Role::create([
            'name' => 'Administrator',
            'code' => 'admin'
        ]);

        Role::create([
            'name' => 'Razrab',
            'code' => 'razrab'
        ]);
        Role::create([
            'name' => 'Team_lead',
            'code' => 'team_lead'
        ]);
        Role::create([
            'name' => 'Analitic',
            'code' => 'anal'
        ]);
        Role::create([
            'name' => 'Owner',
            'code' => 'owner'
        ]);

    }
}
