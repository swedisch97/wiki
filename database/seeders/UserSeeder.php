<?php


namespace Database\Seeders;


use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::factory(10)->create();

        $token = User::query()->first()->createToken('api token');

        Log::info($token);

//        User::create([
//            'name' => 'test user',
//            'email' => 'test@email',
//            'email_verified_at' => now(),
//            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
//            'remember_token' => Str::random(10),
//            'role_id' => Role::first()->id
//            'team_id'=>
//        ]);
    }
}
